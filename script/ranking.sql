SELECT
    ranking_date,
    product_name,
    cate_type,
    category_type,
    device_type,
    ranking
  FROM (
    SELECT
      ranking_date,
      CASE
      -- TrueID --
        WHEN productName = 'TrueID' AND category = 'Overall > Lifestyle' THEN 'TrueID' -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'TrueID' -- TrueID(android)
        WHEN productName = 'TrueID' AND category = 'Overall' THEN 'TrueID' -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL' THEN 'TrueID' -- Android
      -- TrueMusic --
        WHEN productName = 'TrueID Music' AND category = 'Overall > Music' THEN 'TrueID Music' -- TrueID Music(iphone)
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL > APPLICATION > MUSIC_AND_AUDIO' THEN 'TrueID Music' -- TrueID Music(android)
        WHEN productName = 'TrueID Music' AND category = 'Overall'  THEN 'TrueID Music' -- TrueID Music(Overall) iphone
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL'  THEN 'TrueID Music' -- TrueID Music(Overall) Android
      -- TrueID TV --
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall > Entertainment' AND device = 'iphone' THEN 'TrueID TV' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL > APPLICATION > ENTERTAINMENT' AND device = 'android' THEN 'TrueID TV' -- TrueID TV(android)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall' and device = 'iphone' THEN 'TrueID TV' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' and category = 'OVERALL' AND device = 'android' THEN 'TrueID TV' -- TrueID TV(android)
      -- TrueYou --
        WHEN productName = 'TrueYou' AND category = 'Overall > Lifestyle' AND device = 'iphone' THEN 'TrueYou' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'TrueYou' -- Trueyou(android)
        WHEN productName = 'TrueYou' AND category = 'Overall'  THEN 'TrueYou' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL' THEN 'TrueYou' -- Trueyou(android)
      -- True iService --
        WHEN productName = 'True iService' AND category = 'Overall > Utilities' THEN 'True iService' -- True iService(iphone)
        WHEN productName = 'True iService' AND category = 'OVERALL > APPLICATION > TOOLS' THEN 'True iService' -- True iService(android)
        WHEN productName = 'True iService' AND category = 'Overall'  THEN 'True iService' -- True iService(iphone)
        WHEN productName = 'True iService' AND category = 'OVERALL' THEN 'True iService' -- True iService(android)
      END AS product_name,
      CASE
      -- TrueID --
        WHEN productName = 'TrueID' AND category = 'Overall > Lifestyle' THEN 'Category' -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'Category' -- TrueID(android)
        WHEN productName = 'TrueID' AND category = 'Overall' THEN 'Overall' -- TrueID(Overall)
        WHEN productName = 'TrueID' AND category = 'OVERALL' THEN 'Overall'
      -- TrueMusic --
        WHEN productName = 'TrueID Music' AND category = 'Overall > Music' THEN 'Category' -- TrueID Music(iphone)
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL > APPLICATION > MUSIC_AND_AUDIO' THEN 'Category' -- TrueID Music(android)
        WHEN productName = 'TrueID Music' AND category = 'Overall'  THEN 'Overall' -- TrueID Music(Overall) iphone
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL'  THEN 'Overall' -- TrueID Music(Overall) Android
      -- TrueID TV --
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall > Entertainment' AND device = 'iphone' THEN 'Category' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL > APPLICATION > ENTERTAINMENT' AND device = 'android' THEN 'Category' -- TrueID TV(android)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall' AND device = 'iphone' THEN 'Overall' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL' AND device = 'android' THEN 'Overall' -- TrueID TV(android)
      -- TrueYou --
        WHEN productName = 'TrueYou' AND category = 'Overall > Lifestyle' AND device = 'iphone' THEN 'Category' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'Category' -- Trueyou(android)
        WHEN productName = 'TrueYou' AND category = 'Overall' AND device = 'iphone' THEN 'Overall' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL' THEN 'Overall' -- Trueyou(Android)
      -- True iService --
        WHEN productName = 'True iService' AND category = 'Overall > Utilities' THEN 'Category' -- True iService(iphone)
        WHEN productName = 'True iService' AND category = 'OVERALL > APPLICATION > TOOLS' THEN 'Category' -- True iService(android)
        WHEN productName = 'True iService' AND category = 'Overall'  THEN 'Overall' -- True iService(Overall)
        WHEN productName = 'True iService' AND category = 'OVERALL' THEN 'Overall' -- True iService(Overall)
      END AS cate_type,
      CASE
      -- TrueID --
        WHEN productName = 'TrueID' AND category = 'Overall > Lifestyle' THEN UPPER(category2) -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN UPPER(category3) -- TrueID(android)
        WHEN productName = 'TrueID' AND category = 'Overall' THEN UPPER(category1) -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL' THEN UPPER(category1) -- TrueID(Android)
      -- TrueMusic --
        WHEN productName = 'TrueID Music' AND category = 'Overall > Music' THEN UPPER(category2) -- TrueID Music(iphone)
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL > APPLICATION > MUSIC_AND_AUDIO' THEN UPPER(category3) -- TrueID Music(android)
        WHEN productName = 'TrueID Music' AND category = 'Overall'  THEN UPPER(category1) -- TrueID Music(Overall) iphone
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL'  THEN UPPER(category1) -- TrueID Music(Overall) Android
      -- TrueID TV --
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall > Entertainment' AND device = 'iphone' THEN UPPER(category2) -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL > APPLICATION > ENTERTAINMENT' AND device = 'android' THEN UPPER(category3) -- TrueID TV(android)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall' AND device = 'iphone' THEN UPPER(category1) -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL' AND device = 'android' THEN UPPER(category1) -- TrueID TV(android)
      -- TrueYou --
        WHEN productName = 'TrueYou' AND category = 'Overall > Lifestyle' AND device = 'iphone' THEN UPPER(category2) -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN category3 -- Trueyou(android)
        WHEN productName = 'TrueYou' AND category = 'Overall' AND device = 'iphone' THEN UPPER(category1) -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL' THEN UPPER(category1) -- Trueyou(Android)
      -- True iService --
        WHEN productName = 'True iService' AND category = 'Overall > Utilities' THEN UPPER(category2) -- True iService(iphone)
        WHEN productName = 'True iService' AND category = 'OVERALL > APPLICATION > TOOLS' THEN UPPER(category3) -- True iService(android)
        WHEN productName = 'True iService' AND category = 'Overall' THEN UPPER(category1)
        WHEN productName = 'True iService' AND category = 'OVERALL' THEN UPPER(category1) -- True iService(Overall)
      END AS category_type,
      CASE
      -- TrueID --
        WHEN productName = 'TrueID' AND category = 'Overall > Lifestyle' THEN 'iOS' -- TrueID(iphone)
        WHEN productName = 'TrueID' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'Android' -- TrueID(android)
        WHEN productName = 'TrueID' AND category = 'Overall' THEN 'iOS' -- TrueID(Overall)
        WHEN productName = 'TrueID' AND category = 'OVERALL' THEN 'Android'
      -- TrueMusic --
        WHEN productName = 'TrueID Music' AND category = 'Overall > Music' THEN 'iOS' -- TrueID Music(iphone)
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL > APPLICATION > MUSIC_AND_AUDIO' THEN 'Android' -- TrueID Music(android)
        WHEN productName = 'TrueID Music' AND category = 'Overall'  THEN 'iOS' -- TrueID Music(Overall) iphone
        WHEN productName = 'TrueID Music - Free Listening!' AND category = 'OVERALL'  THEN 'Android' -- TrueID Music(Overall) Android
      -- TrueID TV --
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall > Entertainment' AND device = 'iphone' THEN 'iOS' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL > APPLICATION > ENTERTAINMENT' AND device = 'android' THEN 'Android' -- TrueID TV(android)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'Overall' AND device = 'iphone' THEN 'iOS' -- TrueID TV(iphone)
        WHEN productName = 'TrueID TV - Watch TV, Movies, and Live Sports' AND category = 'OVERALL' AND device = 'android' THEN 'Android' -- TrueID TV(android)
      -- TrueYou --
        WHEN productName = 'TrueYou' AND category = 'Overall > Lifestyle' AND device = 'iphone' THEN 'iOS' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL > APPLICATION > LIFESTYLE' THEN 'Android' -- Trueyou(android)
        WHEN productName = 'TrueYou' AND category = 'Overall' AND device = 'iphone' THEN 'iOS' -- Trueyou(iphone)
        WHEN productName = 'TrueYou' AND category = 'OVERALL' THEN 'Android' -- Trueyou(Android)
      -- True iService --
        WHEN productName = 'True iService' AND category = 'Overall > Utilities' THEN 'iOS' -- True iService(iphone)
        WHEN productName = 'True iService' AND category = 'OVERALL > APPLICATION > TOOLS' THEN 'Android' -- True iService(android)
        WHEN productName = 'True iService' AND category = 'Overall'  THEN 'iOS' -- True iService(Overall)
        WHEN productName = 'True iService' AND category = 'OVERALL' THEN 'Android' -- True iService(Overall)
      END AS device_type,
      ranking
    FROM 
      (
      SELECT
        rankTIme AS ranking_date,
        productName,
        device,
        category,
        FIRST(SPLIT(category,' > ')) AS category1,
        NTH(2,SPLIT(category,' > ')) AS category2,
        NTH(3,SPLIT(category,' > ')) AS category3,
        rank AS ranking
      FROM
        [true-dmp:BI_ANALYTICS.product_ranking]
      ORDER BY
        rankTime DESC 
      )
  )
  WHERE
    category_type IS NOT NULL
    AND device_type IS NOT NULL
    and product_name is not NULL
  GROUP BY
    ranking_date,
    product_name,
    cate_type,
    category_type,
    device_type,
    ranking